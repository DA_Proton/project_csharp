﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using projectLib;

namespace projet_Csharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<Tache> taches = TaskList.load();
            foreach(Tache elt in taches)
            {
                TaskGrid.Rows.Add(elt.toRow());
            }
        }

        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            TaskList.save();
            if (MessageBox.Show("Etes-vous certain de vouloir quitter ?", "Quitter",
                MessageBoxButtons.YesNo) == DialogResult.No)
                    e.Cancel = true;
        }
        private void QuitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
                TaskList.save();
                this.Close();
        }

        private void DeleteTaskBtn_Click(object sender, EventArgs e)
        {
            if (TaskGrid.SelectedRows.Count > 0 && TaskGrid.SelectedRows[0].Index != TaskGrid.Rows.Count - 1)
            {
                TaskList.removeAt(TaskGrid.SelectedRows[0].Index);
                TaskGrid.Rows.RemoveAt(TaskGrid.SelectedRows[0].Index);
            }
        }

        private void AddTaskBtn_Click(object sender, EventArgs e)
        {
            if (TaskName.Text != "")
            {
                TaskList.add(new Tache(TaskList.maxId() + 1, TaskName.Text, DateDebPicker.Value, DateFinPicker.Value));
                TaskGrid.Rows.Add(TaskList.getLast().toRow());
                TaskName.Text = "";
            }
            else MessageBox.Show("Entrez un nom de tache");
                
        }

        private void ModifyTaskBtn_Click(object sender, EventArgs e)
        {
            int id = TaskGrid.SelectedRows[0].Index; //plante si on ne selectionne pas de tache :(

            Tache tmp = TaskList.getOne(id); //on recupere la tache

            TaskList.removeAt(id);
            TaskGrid.Rows.RemoveAt(id); //on supprime la tache de la DataGrid et de la liste

            tmp.Modify(TaskName.Text, DateDebPicker.Value, DateFinPicker.Value); //on la modifie

            TaskList.add(tmp);
            TaskGrid.Rows.Add(tmp.toRow()); //on re ajoute la tache
            TaskName.Text = "";

        }

        private void DetailTaskBtn_Click(object sender, EventArgs e)
        {
            int id = TaskGrid.SelectedRows[0].Index;
            Tache tmp = TaskList.getOne(id);
            if(tmp.getDateDeb() > DateTime.Now)
            {
                MessageBox.Show(string.Format("La tache débutera dans {0} jour(s)", 
                    (tmp.getDateDeb() - DateTime.Now).Days+1));
            }
            else
            {
                MessageBox.Show(string.Format("La durée de la tache et de {0} jour(s)\n et il reste {1} jour(s) pour la terminée",
               tmp.Duree().Days, tmp.tpsRestant().Days));
            }
        }
    }
}
